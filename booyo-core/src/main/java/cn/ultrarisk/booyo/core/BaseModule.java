package cn.ultrarisk.booyo.core;

/**
 * BaseComponent
 *
 * @author longlin(lin23871@163.com)
 * @date 2014/9/17
 * @since V1.0
 */
public abstract class BaseModule {
    private String id;
    private String host;
    private Integer port;
    private Integer status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
