package cn.ultrarisk.booyo.core.parser;

import java.io.Serializable;

/**
 * ParseResult
 *
 * @author sevendlong(lin23871@163.com)
 * @date 2014/9/17
 * @since V1.0
 */
public class ParseResult implements Serializable {
    private static final long serialVersionUID = 6101894933263641562L;
}
