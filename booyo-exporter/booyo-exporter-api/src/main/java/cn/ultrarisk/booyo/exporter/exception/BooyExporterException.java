package cn.ultrarisk.booyo.exporter.exception;

/**
 * BooyExporterException
 *
 * @author sevendlong(lin23871@163.com)
 * @date 2014/9/17
 * @since V1.0
 */
public class BooyExporterException extends RuntimeException {
    private static final long serialVersionUID = -4220836922224460289L;

    public BooyExporterException() {
    }

    public BooyExporterException(String message) {
        super(message);
    }

    public BooyExporterException(String message, Throwable cause) {
        super(message, cause);
    }

    public BooyExporterException(Throwable cause) {
        super(cause);
    }
}
