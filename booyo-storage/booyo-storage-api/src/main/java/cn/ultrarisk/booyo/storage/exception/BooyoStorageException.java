package cn.ultrarisk.booyo.storage.exception;

/**
 * BooyoStorageException
 *
 * @author longlin(lin23871@163.com)
 * @date 2014/9/16
 * @since V1.0
 */
public class BooyoStorageException extends RuntimeException {
    private static final long serialVersionUID = -1369764377245873605L;

    public BooyoStorageException() {
    }

    public BooyoStorageException(String message) {
        super(message);
    }

    public BooyoStorageException(String message, Throwable cause) {
        super(message, cause);
    }

    public BooyoStorageException(Throwable cause) {
        super(cause);
    }
}
