package cn.ultrarisk.booyo.storage.entry.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * ApplicationConfig
 *
 * @author longlin(lin23871@163.com)
 * @date 2014/9/16
 * @since V1.0
 */
@Configuration
@ComponentScan(basePackages = "cn.ultrarisk.booyo.storage")
public class ApplicationConfig {
}
